<?php

/**
 *@file
 *Synchronizer admin inc
*/
function synchronizer_admin_settings() {
  //Encuentra todos los tipos de contenido
  $_node_types = node_get_types();
  $node_types = array();
  foreach ($_node_types as $node_type) {
    $node_types[$node_type->type] = $node_type->name;
  }

  //Encuentra todas las taxonomias
  $_vocabularies = taxonomy_get_vocabularies();
  $vocabularies = array();
  foreach ($_vocabularies as $vocabulary) {
        $vocabularies[$vocabulary->vid] = $vocabulary->name;
  }

  $form = array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 60,
    '#description' => t('Nombre del sincronizador. Es utilizado para fines administrativos'),
    '#required' => TRUE,
  );
  $form['general']['host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#size' => 60,
    '#description' => t('Agrega la dirección IP o la url a la que se conectará el Módulo'),
    '#required' => TRUE,
  );
  $form['general']['db_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Database name'),
    '#size' => 60,
    '#description' => t('El nombre de la base de datos.'),
    '#required' => TRUE,
  );
  $form['general']['table'] = array(
    '#type' => 'textfield',
    '#title' => t('Table'),
    '#size' => 60,
    '#description' => t('El nombre de la tabla o la vista a la que se conectará'),
    '#required' => TRUE,
  );
  $form['general']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#size' => 60,
    '#description' => t('El nombre de usuario para poder acceder a la tabla.'),
    '#required' => TRUE,
  );
  $form['general']['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => 60,
    '#description' => t('La contraseña para acceder a la tabla.'),
    '#required' => TRUE,
  );
  $form['general']['limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#size' => 60,
    '#description' => t('Limite dentro del query que se realizará para hacer la sincronización de datos.'),
    '#required' => TRUE,
  );
  $form['db'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Ordenamiento de resultados'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['db']['field'] = array(
    '#type' => 'textfield',
    '#title' => t('Campo'),
    '#size' => 60,
    '#description' => t('El nombre del campo por el que se ordenará la tabla.'),
    '#required' => TRUE,
  );
  $form['db']['order'] = array(
    '#type' => 'radios', 
    '#title' => t('Ordenamiento'), 
    '#options' => array( 'ASC' => t('Ascending'), 'DESC' => t('Descending')),
    '#required' => TRUE,
  );
  $form['db']['db_type'] = array(
    '#type' => 'radios', 
    '#title' => t('Database type'), 
    '#options' => array( 'mysql' => t('MySQL'), 'mssql' => t('MsSQL')),
    '#required' => TRUE,
  );
  $form['vocabularies'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Vocabularies'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['vocabularies']['vid'] = array(
    '#type' => 'checkboxes',
    '#options' => $vocabularies,
    '#title' => t('Vocabularies'),
    '#required' => FALSE,
  );
  $form['node_types'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['node_types']['type'] = array(
    '#type' => 'radios',
    '#options' => $node_types,
    '#required' => TRUE,
    '#title' => t('Content types'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Agregar sincronizador'),
  );  
  return $form;
}

function synchronizer_admin_settings_submit($form, &$form_state) {
  $record = (object) NULL;
  $record->name       = $form_state['values']['name'];
  $record->host       = $form_state['values']['host'];
  $record->db_table   = $form_state['values']['table'];
  $record->user       = $form_state['values']['user'];
  $record->pass       = $form_state['values']['pass'];
  $record->field      = $form_state['values']['field'];
  $record->db_order   = $form_state['values']['order'];
  $record->limite     = $form_state['values']['limit'];
  $record->db_name    = $form_state['values']['db_name'];
  $record->db_type    = $form_state['values']['db_type'];
  $record->node_type  = $form_state['values']['type'];
  drupal_write_record('synchronizer', $record);
  $sid = db_last_insert_id('synchronizer', 'sid');
  $vocabularies = $form_state['values']['vid'];

  foreach ($vocabularies as $vid => $vocabulary) {
    if ( $vocabulary != 0 ) {
      $record = (object) NULL;
      $record->sid = $sid;
      $record->vid = $vid;
      drupal_write_record('synchronizer_vocabularies', $record);
    }
  }
}

function synchronizer_admin_list() {
  $synchronizers = synchronizer_get_all();
  $header = array(t('Name'), t('Type'), array('data' => t('Operations'), 'colspan' => '4'));
  $rows = array();
  foreach ( $synchronizers as $synchronizer ) {
    $row = array();
    $row = array(
      check_plain($synchronizer->name),
      check_plain($synchronizer->node_type),
  );
    $row[] = array('data' => l(t('View'), 'admin/build/synchronizer/' . $synchronizer->sid .'/view' ));
    $row[] = array('data' => l(t('Synchronize'), 'admin/build/synchronizer/' . $synchronizer->sid .'/sync' ));
    $row[] = array('data' => l(t('Vocabularies match'), 'admin/build/synchronizer/' . $synchronizer->sid .'/vocabularies_match' ));
    $row[] = array('data' => l(t('Fields match'), 'admin/build/synchronizer/' . $synchronizer->sid .'/fields_match' ));
    $row[] = array('data' => l(t('Delete'), 'admin/build/synchronizer/' . $synchronizer->sid .'/delete' ));
    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No synchronizers available.'), 'colspan' => '5', 'class' => 'message'));
  }

  return theme('table', $header, $rows);
}

function synchronizer_admin_sync( $form, $sid ) {
  $synchronizer = synchronizer_get_by_id( $sid );
  if ( !$synchronizer ) {
    drupal_goto("admin/build/synchronizer/list");
  }
  $form = array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Actualización manual'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Actualización manual'),
  );  
  $form['sid'] = array('#type' => 'hidden', '#value' => $sid);
  return $form;
}

function synchronizer_admin_vocabularies_match( $form, $sid) {
  $form = array();
  $x = 0;
  $vocabularies = synchronizer_get_vocabularies_by_id( $sid );
  $form['vocabularies'] = array(
    '#tree' => TRUE,
  );
  foreach ( $vocabularies as $vocabulary ) {
    $_vocabulary = taxonomy_vocabulary_load( $vocabulary->vid );
    $form['vocabularies'][$_vocabulary->vid] = array(
      '#type' => 'fieldset',
      '#weight' => -20,
      '#title' => $_vocabulary->name,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
    $form['vocabularies'][$_vocabulary->vid]['sid'] = array('#type' => 'hidden', '#value' => $sid);
    $form['vocabularies'][$_vocabulary->vid]['vid'] = array('#type' => 'hidden', '#value' => $vocabulary->vid);
    $form['vocabularies'][$_vocabulary->vid]['field'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#title' => t('Field'),
      '#default_value' => $vocabulary->field,
      '#description' => t('El nombre del campo de la base de datos externa con el cual se emparejará el vocabulario definido.'),
      '#required' => TRUE,
    );
    $x++;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );  
  return $form;
}

function synchronizer_admin_vocabularies_match_submit( $form, &$form_state) {
  $info = $form_state['values']['vocabularies'];
  foreach ( $info as $i ) {
    $record        = (object) NULL;
    $record->field = $i['field'];
    $record->vid   = $i['vid'];
    $record->sid   = $i['sid'];
    $sid = drupal_write_record('synchronizer_vocabularies', $record, array('vid', 'sid'));
  }
}

function synchronizer_admin_fields_match( $form, $sid) {
  $form = array();
  $fields = synchronizer_get_fields_by_id( $sid );
  $form['fields'] = array(
    '#tree' => TRUE,
  );

  //CCK FIELDS
  foreach ( $fields['fields'] as $field_name => $field ) {
    $form['fields'][$field_name] = array(
      '#type' => 'fieldset',
      '#weight' => -20,
      '#title' => $field['widget']['label'],
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['fields'][$field_name]['sid'] = array('#type' => 'hidden', '#value' => $sid);
    $form['fields'][$field_name]['node_field'] = array('#type' => 'hidden', '#value' => $field_name);
    $form['fields'][$field_name]['field'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#title' => t('Field'),
      '#default_value' => synchronizer_get_field( $sid, $field_name ),
      '#description' => t('El nombre del campo de la base de datos externa con el cual se emparejará el campo del nodo.'),
      '#required' => TRUE,
    );
  }

  //TITLE FIELD
  $field_name = 'title';
  $form['fields'][$field_name] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => $fields['extra'][$field_name]['label'],
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
    $form['fields'][$field_name]['sid'] = array('#type' => 'hidden', '#value' => $sid);
    $form['fields'][$field_name]['node_field'] = array('#type' => 'hidden', '#value' => $field_name);
    $form['fields'][$field_name]['field'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#title' => t('Field'),
      '#default_value' => synchronizer_get_field( $sid, $field_name ),
      '#description' => t('El nombre del campo de la base de datos externa con el cual se emparejará el campo del nodo.'),
      '#required' => TRUE,
    );

  //BODY FIELD
  if ( isset($fields['extra']['body_field']) ) {
  $field_name = 'body_field';
  $form['fields'][$field_name] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => $fields['extra'][$field_name]['label'],
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
    $form['fields'][$field_name]['sid'] = array('#type' => 'hidden', '#value' => $sid);
    $form['fields'][$field_name]['node_field'] = array('#type' => 'hidden', '#value' => $field_name);
    $form['fields'][$field_name]['field'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#title' => t('Field'),
      '#default_value' => synchronizer_get_field( $sid, $field_name ),
      '#description' => t('El nombre del campo de la base de datos externa con el cual se emparejará el campo del nodo.'),
      '#required' => TRUE,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );  
  return $form;
}

function synchronizer_admin_fields_match_submit( $form, &$form_state) {
  $info = $form_state['values']['fields'];
  foreach ( $info as $i ) {
    $record             = (object) NULL;
    $record->field      = $i['field'];
    $record->node_field = $i['node_field'];
    $record->sid        = $i['sid'];
    $field = synchronizer_get_field( $i['sid'], $i['node_field'] );
    if ( !$field ) {
      $sid = drupal_write_record('synchronizer_fields', $record );
    }
    else{
      $sid = drupal_write_record('synchronizer_fields', $record, array('node_field', 'sid'));
     }
  }
}

function synchronizer_admin_sync_submit($form, &$form_state) {
  $sid = $form_state['values']['sid'];
  _synchronizer_sync( $sid );
}
